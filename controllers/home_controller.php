<?php

require_once 'interfaces/controller_interface.php';
require_once 'lib/object_maker.php';
require_once 'views/home/index_view.php';
require_once 'views/home/login_view.php';

require_once 'services/usuario_service.php';

class HomeController implements IController
{

    //============MEMBER PROPERIES=====================
    //============PUBLIC METHODS=======================
    public function executeAction($_actionName)
    {
        switch ($_actionName) {
            case "index":
                $this->index();
                break;
            case "login":
                $this->login();
                break;
            case 'validar_login':
                $this->validateLogin();
                break;
        }
    }

    //============ACTIONS==============================
    private function index()
    {
        $data = null;
        $view = ObjectMaker::getView('index', $data);
        $view->displayHtml();
    }
    private function login()
    {
        $data = null;
        $data['invalid'] = false;
        if (isset($_GET['invalid'])) {
            $data['invalid'] = true;
        }
        $view = ObjectMaker::getView('login', $data);
        $view->displayHtml();
    }
    //
    private function validateLogin(){
        if(isset($_POST['_nombreUsuario']) && isset($_POST['_password'])){
            $nombreUsuario = $_POST['_nombreUsuario'];
            $password =  $_POST['_password'];

            $us = new UsuarioService();
            $u = $us->validar($nombreUsuario,$password);

            if ($u != null) {
                session_start();
                $_SESSION["idUsuario"] = $u->Id;
                $_SESSION["Nombre"] = $u->Nombre . " " . $u->Apellidos;
                $_SESSION["Administrador"] = $u->Administrador;
            }
            else{
                header("location: ?ctrl=home&action=login&invalid=true");
            }
        }
    }
}
