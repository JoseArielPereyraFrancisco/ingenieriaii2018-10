<?php
require_once 'lib/object_maker.php';

class FrontController {
    static function callAction($_controller, $_action) {
        $controller = ObjectMaker::getController($_controller);
        if ($controller != null) {
            $controller->executeAction($_action);
        } else {
            header("location: site_media/html/core/not_found.html");
        }
    }
}
