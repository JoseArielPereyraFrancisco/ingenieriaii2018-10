<?php

require_once 'interfaces/controller_interface.php';
require_once 'lib/object_maker.php';
require_once 'views/estudiantes/index_view.php';

class EstudianteController implements IController
{

    //============MEMBER PROPERIES=====================
    //============PUBLIC METHODS=======================
    public function executeAction($_actionName)
    {
        switch ($_actionName) {
            case "index":
                $this->index();
                break;
        }
    }

    //============ACTIONS==============================
    private function index()
    {
        $data = null;
        $view = ObjectMaker::getView('estudianteIndex', $data);
        $view->displayHtml();
    }
    //
}
