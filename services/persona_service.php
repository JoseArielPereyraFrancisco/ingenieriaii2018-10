<?php
require_once 'models/persona_model.php';
require_once 'lib/data_manager.php';

class PersonaService extends DataManager
{
    public function registrarNuevo($_perona)
    {

    }
    //
    public function buscar($_busqueda){
        $personas = array();
        $sql = "call sp_buscarPersonas('$_busqueda');";

        $result = $this->executeQuery($sql);
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                $p = new Persona();

                $p->Id= $row['Id'];
                $p->Nombre= $row['Nombre'];
                $p->Apellidos= $row['Apellidos'];

                array_push($personas,$p);
            }
        }
        return $personas;
    }
}
