<?php

class ViewRender {

    static function renderTemplate($_filename, $_dictionary) {
        $html = file_get_contents($_filename);
        if ($_dictionary != NULL) {
            foreach ($_dictionary as $clave => $valor) {
                $html = str_replace('{' . $clave . '}', $valor, $html);
            }
        }
        return $html;
    }
    static function getFile($_filename) {
        $heandContent = file_get_contents($_filename);
        return $heandContent;
    }

}
