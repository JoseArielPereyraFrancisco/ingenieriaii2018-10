<?php
class Util{
    static function getJsonFileData($_fileName){
        $jsonStr = file_get_contents($_fileName);

        $decodedjsonStr = html_entity_decode($jsonStr);
        $r = json_decode($decodedjsonStr);
        return $r;
    }
}