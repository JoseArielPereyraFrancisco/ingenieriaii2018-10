<?php
class ObjectMaker
{
    /*Definition of all  controllers to be used*/
    public static function getController($_controllerName)
    {
        require_once "controllers/home_controller.php";
        require_once "controllers/estudiante_controller.php";
        //
        $object = null;
        switch ($_controllerName) {
            case 'home':
                $object = new HomeController();
                break;
            case 'estudiante':
                $object = new EstudianteController();
                break;
        }
        return $object;
    }
    /*Definition of all  views to be used*/
    public static function getView($_viewName, $_data)
    {
        $object = null;
        switch ($_viewName) {
            case 'index':
                $object = new IndexView($_data);
                break;
            case 'login':
                $object = new LoginView($_data);
                break;
            case 'estudianteIndex':
                $object = new EstudianteIndexView($_data);
                break;
        }
        return $object;
    }
}
