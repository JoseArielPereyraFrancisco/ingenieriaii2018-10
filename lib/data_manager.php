<?php
require_once 'lib/util.php';
abstract class DataManager
{
    public function __construct()
    {
        try {
            $data = Util::getJsonFileData("config.json");
            $this->config = $data->ConectionStr;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    //
    private $config;
    private $mysqli;
    //
    public function databaseConnect()
    {
        try {
            $this->mysqli = new mysqli($this->config->host, $this->config->user, $this->config->password, $this->config->dbname, $this->config->port, $this->config->socket);
            $this->mysqli->set_charset("utf8");
        } catch (mysqli_sql_exception $e) {
            echo $e->getMessage();
        }
    }
    //
    public function closeConnection()
    {
        $this->mysqli->close();
    }
    //
    public function executeNonQuery($sql)
    {
        $r = false;
        $this->databaseConnect();

        if ($this->mysqli->query($sql)) {
            $r = true;
        }
        $this->closeConnection();
        return $r;
    }
    //
    public function executeQuery($sql)
    {
        $this->databaseConnect();
        $result = "";
        $this->mysqli->set_charset("utf8");
        $result = $this->mysqli->query($sql);
        return $result;
    }
    //
}
