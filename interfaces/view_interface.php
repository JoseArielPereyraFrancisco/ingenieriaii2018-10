<?php
interface IView {
    public function loadDictionary();
    public function displayHtml();
}
