<?php

require_once 'lib/view_render.php';
require_once 'models/view.php';
require_once 'interfaces/view_interface.php';

class LoginView extends view implements IView{

    public function __construct($_data) {
        $this->data = $_data;
        $header = "";
    }
    //============PUBLIC METHODS=======================
    public function loadDictionary() {
        $invalidErrorMessage = "";
        if ($this->data["invalid"]) {
            $invalidErrorMessage = "<p style=color:red;>Usuario y Contraseña no coinciden</p>";
        }

        $this->dictionary = NULL;
        $headContent = ViewRender::getFile("site_media/html/layout/head.html");
        $this->dictionary = array(
            'HEAD_CONTENT'=>$headContent,
            'LOGIN_MESSAGE'=>$invalidErrorMessage
        );
    }

    public function displayHtml() {
        $this->loadDictionary();
        $html = ViewRender::renderTemplate("site_media/html/home/login.html", $this->dictionary);
        print($html);
    }
}
