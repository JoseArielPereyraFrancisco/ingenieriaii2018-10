<?php

require_once 'lib/view_render.php';
require_once 'models/view.php';
require_once 'interfaces/view_interface.php';

class IndexView extends view implements IView{

    public function __construct($_data) {
        $this->data = $_data;
        $this->header = "";
    }
    //============PUBLIC METHODS=======================
    public function loadDictionary() {
        $this->dictionary = NULL;
        $sidePanelDictionary = array(
            'DASHBOARD_MENU' => "active",
            'ESTUDIANTE_MENU' => ""
        );

        $headContent = ViewRender::getFile("site_media/html/layout/head.html");
        $header = ViewRender::getFile("site_media/html/layout/header.html");
        $sidePanel = ViewRender::renderTemplate("site_media/html/layout/side_panel.html", $sidePanelDictionary);

        $this->dictionary['TITLE'] = "Tablero";
        $this->dictionary['HEAD_CONTENT'] = $headContent;
        $this->dictionary['HEADER'] = $header;
        $this->dictionary['SIDE_PANEL'] = $sidePanel;
    }

    public function displayHtml() {
        $this->loadDictionary();
        $html = ViewRender::renderTemplate("site_media/html/home/index.html", $this->dictionary);
        print($html);
    }

}
