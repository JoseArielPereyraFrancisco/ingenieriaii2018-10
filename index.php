<?php
/*
Autor: Víctor Manuel Brito
Fecha: March 11 2018
*/
require_once 'controllers/front_controller.php';
$controller = "";
$action = "";

if (isset($_GET['ctrl']) && isset($_GET['action'])) {
    $controller = $_GET['ctrl'];
    $action = $_GET['action'];
    FrontController::callAction($controller, $action);
}else{
    header("location: ?ctrl=home&action=login");
}